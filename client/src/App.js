import React from "react";
import "./App.css";
import FileUpload from "./components/FileUpload";
const App = () => (
  <div className="container mt-4">
    <h4 className="display-4 text-center mb-4">React File Upload</h4>
    <p className="display-8 text-center mb-4">
      Choose a File and click on Upload, to upload the File.
    </p>
    <FileUpload />
  </div>
);
export default App;
